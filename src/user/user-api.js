/*
 * Author: vantolbennett
 * Project: mern-project-template
 * File: user-api.js
 * Class: user-api.js
 * Copyright (c) 2019. 
 * <www.tecode.io> 
 */

import axios from 'axios'

const create = async (user) => {
	try {
		const res = await axios('/api/users', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(user)
		});
		return res.json()
	} catch (e) {
		console.log(e)
	}
};

const list = async () => {
	try {
		const res = await axios.get('http://localhost:5000/api/users');
		return res;
	} catch (e) {
		console.log(e)
	}
	// return fetch('http://localhost:5000/api/users', {
	// 	method: 'GET',
	// }).then(response => {
	// 		return response.json()
	// })
	// 	.catch((err) => console.log(err))
};

const read = (params, credentials) => {
	return fetch('/api/users/' + params.userId, {
		method: 'GET',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + credentials.t
		}
	}).then((response) => {
		return response.json()
	}).catch((err) => console.log(err))
}

const update = (params, credentials, user) => {
	return fetch('/api/users/' + params.userId, {
		method: 'PUT',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + credentials.t
		},
		body: JSON.stringify(user)
	}).then((response) => {
		return response.json()
	}).catch((err) => {
		console.log(err)
	})
}

const remove = (params, credentials) => {
	return fetch('/api/users/' + params.userId, {
		method: 'DELETE',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + credentials.t
		}
	}).then((response) => {
		return response.json()
	}).catch((err) => {
		console.log(err)
	})
}

export { create, list, read, update, remove}
