/*
 * Author: vantolbennett
 * Project: mern-project-template
 * File: Users.jsx
 * Class: Users.jsx
 * Copyright (c) 2019. 
 * <www.tecode.io> 
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import  ListItem from '@material-ui/core/ListItem'
import  ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import  ListItemAvatar from '@material-ui/core/ListItemAvatar'
import  ListItemText from '@material-ui/core/ListItemText'
import List from '@material-ui/core/List'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import Person from '@material-ui/core/Paper'
import {Link} from 'react-router-dom'

import {list} from './user-api';



const styles = theme => ({
	root: theme.mixins.gutters({
		padding: theme.spacing.unit,
		margin: theme.spacing.unit * 5
	}),
	title: {
		margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`,
		color: theme.palette.openTitle
	}
});

class Users extends Component {
	state = {
		users: []
	};

	componentDidMount() {
		list().then(user => console.log(user.data))

	}
	render() {
		//const {classes} = this.props;
		const user = this.state.users.map(user =>{
			console.log(user)
		});
		return (
			<div>
				All Users
				{user}
			</div>
		);
	}
}

Users.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Users);
