/*
 * Author: vantolbennett
 * Project: food-event-app
 * File: firebase.util.js
 * Class: firebase.util.js
 * Copyright (c) 2019.
 * <www.tecode.io>
 */

import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const config = {
    apiKey: "AIzaSyA7TACL8WcvuFAQThce-Z27iqitm62zh9Y",
    authDomain: "foodpins-b99ff.firebaseapp.com",
    databaseURL: "https://foodpins-b99ff.firebaseio.com",
    projectId: "foodpins-b99ff",
    storageBucket: "foodpins-b99ff.appspot.com",
    messagingSenderId: "657464617364",
    appId: "1:657464617364:web:218e9258c32bb9d8"
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

/*
    Handle the sign-in flow with the Firebase SDK
 */

//  Create an instance of the Google provider object:
const provider = new firebase.auth.GoogleAuthProvider();

provider.setCustomParameters({prompt: 'select_account'});

export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;