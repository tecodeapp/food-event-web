/*
 * Author: vantolbennett
 * Project: food-event-app
 * File: EventList.jsx
 * Class: EventList.jsx
 * Copyright (c) 2019. 
 * <www.tecode.io> 
 */

import React, {Component} from 'react';

import EventListItem from './EventListItem';
import CardBackground from '../../../components/Card/CardBackground';
import CardDyn from '../../../components/Card/CardDyn';
import SimpleCard from '../../../components/Card/SimpleCard';


class EventList extends Component {
	render() {
		const {events, onEventEdit} = this.props;
		return (
			<div>
				<h1>Event List</h1>
				{events.map(event => {
					return <CardBackground key={event.id} event={event} onEventEdit={onEventEdit}/>
				})}
			</div>
		);
	}
}

export default EventList;