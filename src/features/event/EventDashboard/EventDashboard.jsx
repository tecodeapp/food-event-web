/*
 * Author: vantolbennett
 * Project: food-event-app
 * File: EventDashboard.jsx
 * Class: EventDashboard.jsx
 * Copyright (c) 2019.
 * <www.tecode.io>
 */

import React, {Component} from 'react';

import { Grid } from 'semantic-ui-react'
import PropTypes from 'prop-types';
import cuid from 'cuid';
import EventList from '../EventList/EventList';
import Form from '../../../components/Forms/SimpleForm';
import ProfileCard from '../../../components/Card/ProfileCard';

import { list } from "../../../user/user-api";


const events = [
	{
		id: '1',
		title: 'Trip to Tower of London',
		date: '2018-03-27T11:00:00+00:00',
		category: 'culture',
		description:
			'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sollicitudin ligula eu leo tincidunt, quis scelerisque magna dapibus. Sed eget ipsum vel arcu vehicula ullamcorper.',
		city: 'London, UK',
		venue: "Tower of London, St Katharine's & Wapping, London",
		hostedBy: 'Bob',
		hostPhotoURL: 'https://randomuser.me/api/portraits/men/20.jpg',
		attendees: [
			{
				id: 'a',
				name: 'Bob',
				photoURL: 'https://randomuser.me/api/portraits/men/20.jpg'
			},
			{
				id: 'b',
				name: 'Tom',
				photoURL: 'https://randomuser.me/api/portraits/men/22.jpg'
			}
		]
	},
	{
		id: '2',
		title: 'Trip to Punch and Judy Pub',
		date: '2018-03-28T14:00:00+00:00',
		category: 'drinks',
		description:
			'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sollicitudin ligula eu leo tincidunt, quis scelerisque magna dapibus. Sed eget ipsum vel arcu vehicula ullamcorper.',
		city: 'London, UK',
		venue: 'Punch & Judy, Henrietta Street, London, UK',
		hostedBy: 'Tom',
		hostPhotoURL: 'https://randomuser.me/api/portraits/men/22.jpg',
		attendees: [
			{
				id: 'b',
				name: 'Tom',
				photoURL: 'https://randomuser.me/api/portraits/men/22.jpg'
			},
			{
				id: 'a',
				name: 'Bob',
				photoURL: 'https://randomuser.me/api/portraits/men/20.jpg'
			}
		]
	}
]

class EventDashboard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			events: events,
			isOpen: false,
			selected: null,
			users: []
		}
	}

	componentDidMount() {
		list().then(users => {
			if (!users) {
				console.log("No users data")
			} else {
				this.setState({
					users: users.data
				})
			}
		})
	}

	handleFormOpen = () => {
		this.setState({
			isOpen: true,
			selected: null
		})
	};
	handleFormCancel = () => {
		this.setState({
			isOpen: false
		})
	};

	handleSelectEvent = (eventUpdate) => () => {
		this.setState({
			selected: eventUpdate,
			isOpen: true
		})
	};

	/*
	Create Event
	 */
	handleCreateEvent = (newEvent) => {
		 newEvent.id = cuid();
		 newEvent.hostPhotoURL = '/assets/img/user-regular.svg';
		const updateEvent = [...this.state.events, newEvent];
		this.setState({
			events: updateEvent,
			isOpen: false
		})
	};


	render() {
		// const user = this.state.users.map(user => {
		// 	return (
		// 		<ProfileCard key={user._id} name={user.name} email={user.email} created={user.created}/>
		// 	)
		// });
		const {selected} = this.state;
		return (
			<div className="container">
				<div className="row">
					<div className="col-md-7">
						<EventList onEventEdit={this.handleSelectEvent} events={this.state.events} />
					</div>
					<div className="col-md-5">
						<button onClick={this.handleFormOpen} className="btn btn-raised btn-rose">Create Foodie</button>
						{this.state.isOpen && <Form selected={selected} createEvent={this.handleCreateEvent} handleFormCancel={this.handleFormCancel}/>}
					</div>
				</div>
			</div>
		);
	}
}

EventDashboard.propTypes = {};

export default EventDashboard;
