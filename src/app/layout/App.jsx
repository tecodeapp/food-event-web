import React, {Component} from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { auth } from "../../firebase/firebase.util";

import EventDashboard from '../../features/event/EventDashboard/EventDashboard';
import Navbar from '../../components/Navbar/Navbar';
import Section from '../../components/SectionParallax/SectionPara';
import { Container } from "semantic-ui-react";
import Login from "../Login/Login";
import MainRouter from "../MainRouter";

class App extends Component{

    state = {
        currentUser: null
    };
    unsubscribeAuth = null;

    componentDidMount() {
       this.unsubscribeAuth = auth.onAuthStateChanged(user =>{
            this.setState({
                currentUser: user
            })
            console.log(user)
        })
    }

    componentWillUnmount() {
        this.unsubscribeAuth();
    }

    render() {
        return (
            <BrowserRouter>

            <div>
                <div className="container">
                <Navbar currentUser={this.state.currentUser}/>
                </div>
                <MainRouter/>
            </div>

            </BrowserRouter>
        );
    }
}

export default App;
