/*
 * Author: vantolbennett
 * Project: food-event-app
 * File: MainRouter.jsx
 * Class: MainRouter.jsx
 * Copyright (c) 2019.
 * <www.tecode.io>
 */

import React, {Component} from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from "./Login/Login";
import EventDashboard from "../features/event/EventDashboard/EventDashboard";
import SigninRegister from "./pages/signin/signin-register";
import LandingPage from "./pages/landingpage/landingpage";

class MainRouter extends Component {
	render() {
		return (
			<Switch>
				<div className="main main-raised">
					<div className="container-fluid">
						<Route exact path="/" component={LandingPage}/>
						<Route path="/login" component={Login}/>
						<Route path="/signin" component={SigninRegister}/>
						<Route path="/event" component={EventDashboard}/>
					</div>
				</div>

			</Switch>
		);
	}
}

export default MainRouter;