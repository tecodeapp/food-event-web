import React, {Component} from 'react';

class Login extends Component {
	render() {
		return (
			<div className="page-header header-filter"
			     style={{backgroundImage: "url('../assets/img/bg7.jpg')", backgroundSize: "cover", backgroundPosition: "top center"}}>
				<div className="container">
					<div className="row">
						<div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
							<div className="card card-signup">
								<form className="form" method="" action="">
									<div className="header header-primary text-center">
										<h4 className="card-title">Log in</h4>
										<div className="social-line">
											<a href="#pablo" className="btn btn-just-icon btn-simple">
												<i className="fa fa-facebook-square"></i>
											</a>
											<a href="#pablo" className="btn btn-just-icon btn-simple">
												<i className="fa fa-twitter"></i>
											</a>
											<a href="#pablo" className="btn btn-just-icon btn-simple">
												<i className="fa fa-google-plus"></i>
											</a>
										</div>
									</div>
									<p className="description text-center">Or Be Classical</p>
									<div className="card-content">

										<div className="input-group">
									<span className="input-group-addon">
										<i className="material-icons">face</i>
									</span>
											<input type="text" className="form-control" placeholder="First Name..."/>
										</div>

										<div className="input-group">
									<span className="input-group-addon">
										<i className="material-icons">email</i>
									</span>
											<input type="text" className="form-control" placeholder="Email..."/>
										</div>

										<div className="input-group">
									<span className="input-group-addon">
										<i className="material-icons">lock_outline</i>
									</span>
											<input type="password" placeholder="Password..." className="form-control"/>
										</div>

										{/*// <!-- If you want to add a checkbox to this form, uncomment this code*/}
										{/*//*/}
										{/*// <div class="checkbox">*/}
										{/*// 	<label>*/}
										{/*// 		<input type="checkbox" name="optionsCheckboxes" checked>*/}
										{/*// 		Subscribe to newsletter*/}
										{/*// 	</label>*/}
										{/*// </div> -->*/}
									</div>
									<div className="footer text-center">
										<a href="#pablo" className="btn btn-primary btn-simple btn-wd btn-lg">Get
											Started</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

				);
	}
}

export default Login;