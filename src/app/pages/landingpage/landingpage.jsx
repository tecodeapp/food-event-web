/*
 * Author: vantolbennett
 * Project: food-event-app
 * File: landingpage.jsx
 * Class: landingpage.jsx
 * Copyright (c) 2019.
 * <www.tecode.io>
 */

import React from 'react';
import Section from "../../../components/SectionParallax/SectionPara";

const LandingPage = () => {
    return (
        <div>
            <Section/>
        </div>
    );
};

export default LandingPage;