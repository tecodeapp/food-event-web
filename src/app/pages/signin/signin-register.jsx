/*
 * Author: vantolbennett
 * Project: food-event-app
 * File: signin-register.jsx
 * Class: signin-register.jsx
 * Copyright (c) 2019. 
 * <www.tecode.io> 
 */

import React from 'react';
import './sign-register.scss'
import SigninComponent from "../../../components/Signin/Signin-Component";

const SigninRegister = () => {
    return (
        <div className="main" style={{marginTop: 200}}>
            <div className="section" >
                <p>Sign in and Register</p>
                <SigninComponent/>

            </div>
        </div>
    );
};

export default SigninRegister;