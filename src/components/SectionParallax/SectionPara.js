/*
 * Author: vantolbennett
 * Project: live-tours
 * File: SectionPara.js
 * Class: SectionPara.js
 * Copyright (c) 2019.
 * <www.tecode.io>
 */

import React from 'react';

const logo = 'https://www.bakingmad.com/BakingMad/media/content/Recipes/Cakes/Trick-or-treat-chocolate-pinata-cake/1-Chocolate-MM-Cake-web.jpg';
const Section = () => {
	return (
		<div className="page-header header-filter" data-parallax="true"
		     style={{backgroundImage: `url(${logo})`}}>
			<div className="container">
				<div className="row">
					<div className="col-md-6">
						<h1 className="title">Good food can be a social event.</h1>
						<h4>Everything is sweet and nice. Lets meet up and discuss it.</h4>
						<br/>

					</div>
				</div>
			</div>
		</div>
	);
};

export default Section;
