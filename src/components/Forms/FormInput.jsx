/*
 * Author: vantolbennett
 * Project: food-event-app
 * File: FormInput.jsx
 * Class: FormInput.jsx
 * Copyright (c) 2019. 
 * <www.tecode.io> 
 */

import React from 'react';
import './form-input.scss'

const FormInput = ({handleChange, label, ...otherProps}) => {
    return (
        <div className="group">
            <input className="form-input" onChange={handleChange} {...otherProps}/>
            {
                label ? <label className={`${otherProps.value.length ? 'shrink' : ''} form-input`}>{label}</label> : null
            }
        </div>
    );
};

export default FormInput;