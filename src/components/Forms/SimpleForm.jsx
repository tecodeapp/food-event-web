/*
 * Author: vantolbennett
 * Project: food-event-app
 * File: SimpleForm.jsx
 * Class: SimpleForm.jsx
 * Copyright (c) 2019.
 * <www.tecode.io>
 */

import React, {Component} from 'react';

/**
 *
 * @type {{date: string, venue: string, author: string, description: string, title: string}}
 */
const emptyEvent = {
	title: '',
	description: '',
	date: '',
	venue: '',
	author: ''
}

class SimpleForm extends Component {

	/**
	 *
	 * @type {{event: {date: string, venue: string, author: string, description: string, title: string}}}
	 */
	state = {
		event: emptyEvent
	};

	componentDidMount() {
		if (this.props.selected !== null) {
			this.setState({
				event: this.props.selected
			})
		}
	}

	componentWillReceiveProps(nextProps, nextContext) {
	}

	/**
	 *
	 * @param event
	 */
	onFormSubmit = (event) => {
		event.preventDefault();
		this.props.createEvent(this.state.event)
	};

	/**
	 *
	 * @param event
	 */
	onInputChange = (event) => {
		const newEvent = this.state.event;
		newEvent[event.target.name] = event.target.value;
		this.setState({
			event: newEvent
		})
	};


	render() {
		const {handleFormCancel} = this.props;
		const {event} = this.state;
		return (
			<div className="" style={{width: "200"}}>
			<form onSubmit={this.onFormSubmit}>
				<div className="form-group">
					<div className="card card-form-horizontal">
					<label>Title</label>
					<input type="text" className="form-control"
					       name="title"
					       onChange={this.onInputChange}
					       value={event.title}
					       placeholder="Title"/>
						<label>Description</label>
					<input type="text" className="form-control"
					       name="description"
					       onChange={this.onInputChange}
					       value={event.description}
					       placeholder="description"/>
						<label>Venue</label>
					<input type="text" className="form-control"
					       name="venue"
					       onChange={this.onInputChange}
					       value={event.venue}
					       placeholder="venue"/>
						<label>Author</label>
					<input type="text" className="form-control"
					       name="author"
					       onChange={this.onInputChange}
					       value={event.author}
					       placeholder="author"/>
					<div className="row">
						<div className="col-sm-3">
							<button className="btn btn-primary btn-raised">Submit</button>
						</div>
						<div className="col-sm-2">
							<button className="btn btn-danger btn-raised"
							        onClick={handleFormCancel}>Cancel</button>
						</div>
					</div>
					</div>
				</div>
			</form>
			</div>

		);
	}
}

export default SimpleForm;