/*
 * Author: vantolbennett
 * Project: live-tours
 * File: Navbar.js
 * Class: Navbar.js
 * Copyright (c) 2019. 
 * <www.tecode.io> 
 */

import React from 'react';
import { Link } from "react-router-dom";
import { auth } from "../../firebase/firebase.util";
import Login from '../../app/Login/Login'

import { withRouter } from 'react-router-dom';

const Navbar = (props) => {
	const {currentUser} = props;
	return (
		<nav className="navbar fixed-top navbar-expand-lg"
		     color-on-scroll="100" id="sectionsNav">
			<div className="container">
				<div className="navbar-translate">
					<Link className="navbar-brand" to="/">
						Food-events </Link>
					<button className="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false"
					        aria-label="Toggle navigation">
						<span className="sr-only">Toggle navigation</span>
						<span className="navbar-toggler-icon"></span>
						<span className="navbar-toggler-icon"></span>
						<span className="navbar-toggler-icon"></span>
					</button>
				</div>
				<div className="collapse navbar-collapse">
					<ul className="navbar-nav ml-auto">
						<li className="nav-item active">
							<Link className="nav-link" href="#" to="/event">
								Events
							</Link>
						</li>

						<li className="nav-item left">
							<a className="nav-link">
								Home
							</a>
						</li>
						{
							currentUser ?  <li className="nav-item">
								<a href="#pablo" className="nav-link" to="/signin" alt="login" data-toggle="modal"
								   data-target="#loginModal" rel="tooltip" title="" data-placement="bottom"
								   data-original-title="SignOut" onClick={() => auth.signOut()}>
									<i className="material-icons">face</i>
								</a>
							</li> : <li className="nav-item right active">
								<Link className="nav-link" to="/signin" alt="login" data-toggle="modal"
									  data-target="#loginModal" rel="tooltip" title="" data-placement="bottom"
									  data-original-title="SignIn">
									<i className="material-icons">fingerprint</i>
								</Link>
							</li>
						}

						<li className="nav-item">
							<a href="#pablo" className="nav-link" to="/signin" alt="login" data-toggle="modal"
							   data-target="#loginModal" rel="tooltip" title="" data-placement="bottom"
							   data-original-title="Profile">
								<i className="material-icons">account_circle</i>
							</a>
						</li>
						{/*<li className="dropdown nav-item">*/}
						{/*	<a href="#" className="dropdown-toggle nav-link" data-toggle="dropdown">*/}
						{/*		<i className="material-icons">apps</i> Components*/}
						{/*	</a>*/}
						{/*	<div className="dropdown-menu dropdown-with-icons">*/}
						{/*		<a href="../index.html" className="dropdown-item">*/}
						{/*			<i className="material-icons">layers</i> All Components*/}
						{/*		</a>*/}
						{/*		<a href="https://demos.creative-tim.com/material-kit/docs/2.1/getting-started/introduction.html"*/}
						{/*		   className="dropdown-item">*/}
						{/*			<i className="material-icons">content_paste</i> Documentation*/}
						{/*		</a>*/}
						{/*	</div>*/}
						{/*</li>*/}
						{/*<li className="nav-item">*/}
						{/*	<a className="nav-link" href="javascript:void(0)" onClick="scrollToDownload()">*/}
						{/*		<i className="material-icons">cloud_download</i> Download*/}
						{/*	</a>*/}
						{/*</li>*/}
						<li className="nav-item">
							<a className="nav-link" rel="tooltip" title="" data-placement="bottom"
							   href="https://twitter.com/CreativeTim" target="_blank"
							   data-original-title="Follow us on Twitter">
								<i className="fa fa-twitter"/>
							</a>
						</li>
						<li className="nav-item">
							<a className="nav-link" rel="tooltip" title="" data-placement="bottom"
							   href="https://www.facebook.com/CreativeTim" target="_blank"
							   data-original-title="Like us on Facebook">
								<i className="fa fa-facebook-square"></i>
							</a>
						</li>
						<li className="nav-item">
							<a className="nav-link" rel="tooltip" title="" data-placement="bottom"
							   href="https://www.instagram.com/CreativeTimOfficial" target="_blank"
							   data-original-title="Follow us on Instagram">
								<i className="fa fa-instagram"></i>
							</a>
						</li>

					</ul>
				</div>
			</div>
		</nav>
	);
};

export default withRouter(Navbar);
