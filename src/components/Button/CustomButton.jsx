/*
 * Author: vantolbennett
 * Project: food-event-app
 * File: CustomButton.jsx
 * Class: CustomButton.jsx
 * Copyright (c) 2019. 
 * <www.tecode.io> 
 */

import React from 'react';
import './custom-button.scss'

const CustomButton = ({children, ...otherProps}) => {
    // TODO: Fix styling of Custom button to reflect sign method of Google
    return (
        <button className="custom-button" {...otherProps}>
            {children}
        </button>
    );
};

export default CustomButton;