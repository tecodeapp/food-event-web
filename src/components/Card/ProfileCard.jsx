import React, {Component} from 'react';
import PropTypes from 'prop-types';

class ProfileCard extends Component {
	render() {
		const {name, email, created} = this.props;
		return (
			<div className="container">
				<div className="row">
					<div className="col-md-6">
						<div className="card card-profile card-plain">
							<div className="row">
								<div className="col-md-5">
									<div className="card-header card-header-image">
										<a href="#pablo">
											<img className="img"
											     src="https://randomuser.me/api/portraits/men/53.jpg"/>
										</a>
										<div className="colored-shadow"
										     style={{backgroundImage: "url(https://randomuser.me/api/portraits/men/53.jpg)", opacity: 1}}></div>
									</div>
								</div>
								<div className="col-md-7">
									<div className="card-body">
										<h4 className="card-title">{name}</h4>
										<h6 className="card-category text-muted">Founder</h6>
										<p className="card-description">
											{email}
											{created}
										</p>
									</div>
									<div className="card-footer">
										<a href="#pablo" className="btn btn-just-icon btn-link btn-twitter"><i
											className="fa fa-twitter"></i></a>
										<a href="#pablo" className="btn btn-just-icon btn-link btn-facebook"><i
											className="fa fa-facebook-square"></i></a>
										<a href="#pablo" className="btn btn-just-icon btn-link btn-google"><i
											className="fa fa-google"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		);
	}
}

ProfileCard.propTypes = {};

export default ProfileCard;
