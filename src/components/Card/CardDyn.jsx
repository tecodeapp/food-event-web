/*
 * Author: vantolbennett
 * Project: food-event-app
 * File: CardDyn.jsx
 * Class: CardDyn.jsx
 * Copyright (c) 2019.
 * <www.tecode.io>
 */

import React, {Component} from 'react';

class CardDyn extends Component {
	render() {
		return (
			<div className="col-md-5">
				<div className="card card-blog">
					<div className="card-image">
						<a href="#pablo">
							<img src="https://iwashyoudry.com/wp-content/uploads/2018/11/Classic-French-Toast-Recipe-3-600x836.jpg" alt=""/>
						</a>
					</div>
					<div className="card-content">
						<h6 className="category text-warning">Dynamic Shadows</h6>
						<h4 className="card-title">
							<a href="#pablo">The image from this card is getting a yellow shadow</a>
						</h4>
					</div>

				</div>
			</div>
		);
	}
}

export default CardDyn;