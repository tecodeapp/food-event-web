/*
 * Author: vantolbennett
 * Project: food-event-app
 * File: CardBackground.jsx
 * Class: CardBackground.jsx
 * Copyright (c) 2019.
 * <www.tecode.io>
 */

import React, {Component} from 'react';

class CardBackground extends Component {
	render() {
		const {event, onEventEdit} = this.props;
		return (
			<div className="col-md-6">
				<div className="card card-background"
				     style={{backgroundImage: "url(https://www.eggs.ca/assets/RecipePhotos/Fluffy-Pancakes-New-CMS.jpg)",
					     color: "white"
				     }}>
					<div className="card-content">
						<h3 className="card-title">{event.title}</h3>
						<h4 className="card-description" style={{color: "white"}}>
							{event.description}
						</h4>
						<div className="author">
							<a href="#pablo">
								<img src={event.hostPhotoURL} alt="..." className="avatar img-raised"/>
								<span style={{color: "white"}}>{event.hostedBy}</span>
							</a>
						</div>
						<h4 className="card-footer card-text">
							{event.date}
						</h4>
						<button className="btn-primary btn-raised" onClick={onEventEdit(event)}>View Event</button>
					</div>
				</div>
			</div>
		);
	}
}

export default CardBackground;