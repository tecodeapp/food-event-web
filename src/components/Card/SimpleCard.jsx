/*
 * Author: vantolbennett
 * Project: food-event-app
 * File: SimpleCard.jsx
 * Class: SimpleCard.jsx
 * Copyright (c) 2019.
 * <www.tecode.io>
 */

import React, {Component} from 'react';

class SimpleCard extends Component {
	render() {
		const {event} = this.props;
		console.log(event);
		return (
			<div className="card">
				<div className="card-content content-info">
					<h5 className="category-social">
						<i className="fa fa-twitter"></i> Twitter
					</h5>
					<h4 className="card-title">
						<a href="#pablo">"You Don't Have to Sacrifice Joy to Build a Fabulous Business and Life"</a>
					</h4>
					<div className="form-group">
						<label className="label-control">Date Picker</label>
						<input type="text" className="form-control datepicker" value="10/10/2016"/>
					</div>
					<div className="footer">
						<div className="author">
							<a href="#pablo">
								<img src="" alt="..." className="avatar img-raised"/>
									<span>Tania Andrew</span>
							</a>
						</div>
						<div className="stats">
							<i className="material-icons">favorite</i> 2.4K &middot;
							<i className="material-icons">share</i> 45
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default SimpleCard;