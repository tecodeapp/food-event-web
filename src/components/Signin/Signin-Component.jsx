/*
 * Author: vantolbennett
 * Project: food-event-app
 * File: Signin-Component.jsx
 * Class: Signin-Component.jsx
 * Copyright (c) 2019.
 * <www.tecode.io>
 */

import React, {Component} from 'react';
import './SignIn.scss'
import FormInput from "../Forms/FormInput";
import CustomButton from "../Button/CustomButton";
import { signInWithGoogle } from "../../firebase/firebase.util";

class SigninComponent extends Component {
    state = {
      email: '',
      password: '',
    };

    handleSubmit = event => {
        event.preventDefault();

        this.setState({
            email: '',
            password: ''
        })
    };
    handleChange = event => {
        const {name, value} = event.target;
        this.setState({
            [name]: value
        })
    };

    render() {
        return (
            <div className='sign-in'>
                <h2>Sign in to your Account</h2>
                <span>Already have a account?</span>
                <form className="form form-control" onSubmit={this.handleSubmit}>
                    <FormInput name="email"
                           type="email"
                           value={this.state.email}
                               label='email'
                           handleChange={this.handleChange}
                           required
                    />
                    <FormInput name="password"
                           type="password"
                               label='email'
                           handleChange={this.handleChange}
                           value={this.state.password}
                           required/>
                    <CustomButton type="submit" name="submit">Signin</CustomButton>
                    <CustomButton onClick={signInWithGoogle}>Google</CustomButton>
                </form>
            </div>
        );
    }
}

export default SigninComponent;